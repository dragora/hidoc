"""
Django settings for project project.

Generated by 'django-admin startproject' using Django 1.8.3.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.8/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
from googleapiclient.discovery import build
import os
import simplejson as json
import httplib2
from oauth2client.client import SignedJwtAssertionCredentials
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
from oauth2client.client import flow_from_clientsecrets
from django.core.urlresolvers import reverse_lazy

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'n&%)mqo2o89e)(5c=w3sxo324e!%k1(d)*b5ed=x3)dd19l046'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Change the URI in order to use the service. You should register your project at the Google Developer Console
# with the correct domain name and write it down here.
ALLOWED_HOSTS = ['http://mysweetdomain.com']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'hidoc',
    'apps.google_api',
    'apps.docs',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'project.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'project.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'hidoc',
        'USER': 'postgres',
        'PASSWORD': 'Somesecretpass10',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}


# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'ru-RU'

TIME_ZONE = 'Europe/Moscow'

USE_I18N = True

USE_L10N = True

USE_TZ = True

LOGIN_URL = '/auth/login/'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, "media")

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = os.path.abspath(os.path.join(BASE_DIR, '..', 'static_content', 'static'))

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

STATICFILES_DIRS = ( os.path.join(BASE_DIR, 'static'), )

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    #     'django.template.loaders.eggs.Loader',
)

# Google API settings
GOOGLE_API_KEY_PATH_SERVICE = os.path.join(BASE_DIR, 'project', 'keys', 'private_key.json')
GOOGLE_API_KEY_PATH_WEB = os.path.join(BASE_DIR, 'project', 'keys', 'client_secret.json')
with open(GOOGLE_API_KEY_PATH_SERVICE) as pk_file:
    pk_data = json.loads(pk_file.read())

email = pk_data['client_email']
key = pk_data['private_key']

GOOGLE_API_FLOW_WEB = flow_from_clientsecrets(
    GOOGLE_API_KEY_PATH_WEB,
    scope='https://www.googleapis.com/auth/drive',
    redirect_uri=''.join([ALLOWED_HOSTS[0], '/auth/oauth2callback'])
    # It could be reverse here to keep things DRY, but the apps are not initialized at the moment we need URL.
)
GOOGLE_API_FLOW_WEB.params['access_type'] = 'offline'
GOOGLE_API_FLOW_WEB.params['approval_prompt'] = 'force'

GOOGLE_API_CREDENTIALS_SERVICE = SignedJwtAssertionCredentials(email, key, scope='https://www.googleapis.com/auth/drive')
GOOGLE_API_CREDENTIALS_SERVICE = GOOGLE_API_CREDENTIALS_SERVICE.authorize(httplib2.Http())

GOOGLE_DRIVE_SERVICE = build('drive', 'v2', http=GOOGLE_API_CREDENTIALS_SERVICE)