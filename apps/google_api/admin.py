# coding=utf-8

from django.contrib import admin
from models import CredentialsModel

__author__ = 'dragora'


class CredentialsAdmin(admin.ModelAdmin):
    pass


admin.site.register(CredentialsModel, CredentialsAdmin)