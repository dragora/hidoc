# coding=utf-8

from django.contrib.auth.models import User
from django.db import models

__author__ = 'dragora'

from oauth2client.django_orm import FlowField
from oauth2client.django_orm import CredentialsField


class CredentialsModel(models.Model):
    id = models.OneToOneField(User, primary_key=True)
    credential = CredentialsField()
