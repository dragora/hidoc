# coding=utf-8

from googleapiclient import errors
from googleapiclient.http import MediaFileUpload

__author__ = 'dragora'


def insert_file(service, title, mime_type, parent_id=None):
    """Insert new file.

    Args:
      service: Drive API service instance.
      title: Title of the file to insert, including the extension.
      description: Description of the file to insert.
      parent_id: Parent folder's ID.
      mime_type: MIME type of the file to insert.
      filename: Filename of the file to insert.
    Returns:
      Inserted file metadata if successful, None otherwise.
    """
    body = {
        'title': title,
        'mimeType': mime_type
    }
    # Set the parent folder.
    if parent_id:
        body['parents'] = [{'id': parent_id}]

    try:
        new_file = service.files().insert(
            body=body,
        ).execute()

        return new_file
    except errors.HttpError, error:
        print 'An error occurred: {}'.format(error)
        return None


def share_file_with_user_id(service, file_id, user_id):
    """Shares the file with the user provided

    Args:
      service: Drive API service instance.
      file_id: ID of the file to insert permission for.
      file_id: ID of the user to insert permission for.

    Returns:
      The inserted permission if successful, None otherwise.
    """
    new_permission = {
        'type': "user",
        'role': "writer",
        'withLink': True,
        'id': user_id
    }
    try:
        return service.permissions().insert(
            fileId=file_id, body=new_permission).execute()
    except errors.HttpError, error:
        print 'An error occurred: {}'.format(error)
    return None


def retrieve_permissions(service, file_id):
    """Retrieve a list of permissions.

    Args:
      service: Drive API service instance.
      file_id: ID of the file to retrieve permissions for.
    Returns:
      List of permissions.
    """
    try:
        permissions = service.permissions().list(fileId=file_id).execute()
        return permissions.get('items', [])
    except errors.HttpError, error:
        print 'An error occurred: %s' % error
    return None


def remove_permission(service, file_id, permission_id):
    """Remove a permission.

    Args:
      service: Drive API service instance.
      file_id: ID of the file to remove the permission for.
      permission_id: ID of the permission to remove.
    """
    try:
        service.permissions().delete(
            fileId=file_id, permissionId=permission_id).execute()
    except errors.HttpError, error:
        print 'An error occurred: %s' % error


def get_user_data_plus(service, user_id='me'):
    people_resource = service.people()
    people_document = people_resource.get(userId=user_id).execute()

    return people_document


def get_user_data_drive(service):
    try:
        about = service.about().get().execute()
        return about
    except errors.HttpError, error:
        print 'An error occurred: %s' % error
    return None