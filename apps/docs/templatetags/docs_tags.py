# coding=utf-8
from django import template
from django.core.exceptions import ObjectDoesNotExist

__author__ = 'dragora'

register = template.Library()


@register.filter
def in_users_allowed(m2m, user):
    try:
        result = m2m.get(pk=user.pk)
    except ObjectDoesNotExist:
        return False
    return True