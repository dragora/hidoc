# coding=utf-8

import bl
from models import DocModel

from django.db.models.signals import m2m_changed

__author__ = 'dragora'


def update_google_doc_users_allowed(sender, instance, action, **kwargs):
    if action == 'post_add':
        bl.api.share_file_with_users(instance)
    if action == 'pre_clear':
        bl.api.reset_file_permissions(instance.api_id)


m2m_changed.connect(
    update_google_doc_users_allowed, sender=DocModel.users_allowed.through, dispatch_uid="update_google_doc_m2m")