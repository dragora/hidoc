# coding=utf-8
from django.shortcuts import render, redirect, resolve_url, get_object_or_404
from django.views.generic import TemplateView

from forms import NewDocForm
from models import DocModel

from oauth2client.django_orm import Storage
from apps.google_api.models import CredentialsModel
from oauth2client import xsrfutil
import httplib2

from django.conf import settings

__author__ = 'dragora'


class DocsView(TemplateView):
    def get(self, request, *args, **kwargs):

        # Google API data
        storage = Storage(CredentialsModel, 'id', request.user, 'credential')
        credential = storage.get()
        if credential is None or credential.invalid:
            settings.GOOGLE_API_FLOW_WEB.params['state'] = xsrfutil.generate_token(settings.SECRET_KEY, request.user)
            authorize_url = settings.GOOGLE_API_FLOW_WEB.step1_get_authorize_url()
            return redirect(authorize_url)
        else:
            http = httplib2.Http()
            http = credential.authorize(http)

            docs = DocModel.objects.all().order_by('-timestamp')

            return render(request, 'pages/docs/docs.html', {
                'section': 'docs',

                'docs': docs,
            })


class NewDocView(TemplateView):
    def get(self, request, *args, **kwargs):
        form = NewDocForm()

        return render(request, 'pages/docs/new_doc.html', {
            'section': 'docs',

            'form': form,
        })

    def post(self, request, *args, **kwargs):
        form = NewDocForm(request.POST)

        if form.is_valid():
            doc = form.save()

            return redirect(resolve_url('docs'))
        else:
            return render(request, 'pages/docs/new_doc.html', {
                'section': 'docs',

                'form': form,
            })


class EditDocView(TemplateView):
    def get(self, request, doc_pk, *args, **kwargs):
        doc = get_object_or_404(DocModel, pk=doc_pk)
        form = NewDocForm(instance=doc)

        return render(request, 'pages/docs/edit_doc.html', {
            'section': 'docs',

            'form': form,
            'doc_name': doc.name
        })

    def post(self, request, doc_pk, *args, **kwargs):
        doc = get_object_or_404(DocModel, pk=doc_pk)
        form = NewDocForm(request.POST, instance=doc)

        if form.is_valid():
            doc = form.save()

            return redirect(resolve_url('docs'))
        else:
            return render(request, 'pages/docs/new_doc.html', {
                'section': 'docs',

                'form': form,
            })