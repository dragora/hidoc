# coding=utf-8

from django.forms import ModelForm
from models import DocModel
from django.core.exceptions import ValidationError

__author__ = 'dragora'


class NewDocForm(ModelForm):
    class Meta:
        model = DocModel
        fields = ['name', 'users_allowed']

    def clean(self):
        users_allowed = self.cleaned_data.get('users_allowed')
        if not users_allowed:
            raise ValidationError(u'Документ должен быть доступен как минимум одному пользователю!')
        if users_allowed.count() > 5:
            raise ValidationError(u"Число пользователей должно быть от не меньше 5!")
        return self.cleaned_data