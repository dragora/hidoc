# coding=utf-8
from django.db import models
from django.contrib.auth.models import User
import bl

from model_utils import FieldTracker

__author__ = 'dragora'


class DocModel(models.Model):
    name = models.CharField(verbose_name=u'Название', max_length=64)
    users_allowed = models.ManyToManyField(User, verbose_name=u'Доступ')
    timestamp = models.DateTimeField(verbose_name=u'Обновлено', auto_now=True)
    api_id = models.CharField(max_length=128, blank=True)
    api_url = models.URLField(u'URL', max_length=128, blank=True)

    tracker = FieldTracker()

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        old_api_id = self.tracker.previous('api_id')
        old_api_url = self.tracker.previous('api_url')

        # Creating file on the Google Drive with <name> name
        if not old_api_id:
            new_file_metadata = bl.api.create_file(self.name)
            if new_file_metadata:
                print 'New Google Drive file created! ID:\n', new_file_metadata['id']
                self.api_id = new_file_metadata['id']
                self.api_url = new_file_metadata['alternateLink']
        else:
            self.api_id = old_api_id
            self.api_url = old_api_url

        super(DocModel, self).save(*args, **kwargs)