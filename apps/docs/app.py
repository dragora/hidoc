# coding=utf-8

from django.apps import AppConfig

__author__ = 'dragora'


class AppConfig(AppConfig):

    name = 'apps.docs'
    verbose_name = 'Docs App'

    def ready(self):

        import signals