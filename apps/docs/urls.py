"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib.auth.decorators import login_required
from django.views.generic import RedirectView

from views import DocsView, NewDocView, EditDocView

urlpatterns = [
    url(r'^$', RedirectView.as_view(url='all', permanent=True), name='docs'),
    url(r'^all/$', login_required(DocsView.as_view()), name='docs_all'),
    url(r'^new/$', login_required(NewDocView.as_view()), name='new_doc'),
    url(r'^edit/(?P<doc_pk>[\w\d-]+)/$', login_required(EditDocView.as_view()), name='edit_doc'),
]
