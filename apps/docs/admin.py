# coding=utf-8

from django.contrib import admin
from models import DocModel
from forms import NewDocForm

__author__ = 'dragora'


class CredentialsAdmin(admin.ModelAdmin):
    form = NewDocForm


admin.site.register(DocModel, CredentialsAdmin)