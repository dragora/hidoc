# coding=utf-8
from apps.google_api.bl import api_calls
from django.conf import settings

from apps.google_api.models import CredentialsModel

from oauth2client.django_orm import Storage
from oauth2client import xsrfutil
import httplib2
from googleapiclient.discovery import build

__author__ = 'dragora'


def create_file(title):
    return api_calls.insert_file(
        settings.GOOGLE_DRIVE_SERVICE, title=title, mime_type='application/vnd.google-apps.document',
    )


def share_file_with_users(doc_instance):

    print 'Sharing the file with users associated!'
    print 'Doc name:', doc_instance.name
    for user in doc_instance.users_allowed.all():
        storage = Storage(CredentialsModel, 'id', user, 'credential')
        credential = storage.get()
        if credential is None or credential.invalid:
            # It may be a good idea to do some kind of job here to solve the wrong credentials problem
            # or at least to let the service administration know about the issue.
            pass
        else:
            http = httplib2.Http()
            http = credential.authorize(http)
            service = build("drive", "v2", http=http)

            user_data = api_calls.get_user_data_drive(service)
            if user_data:
                api_calls.share_file_with_user_id(
                    settings.GOOGLE_DRIVE_SERVICE, doc_instance.api_id, user_data['permissionId']
                )
            else:
                print 'The user haven\'t got appropriate permissions! Skipping.'


def get_file_permissions(file_id):
    return api_calls.retrieve_permissions(settings.GOOGLE_DRIVE_SERVICE, file_id)


def reset_file_permissions(file_id):
    for permission in get_file_permissions(file_id):
        if permission['role'] != 'owner':
            result = api_calls.remove_permission(settings.GOOGLE_DRIVE_SERVICE, file_id, permission['id'])