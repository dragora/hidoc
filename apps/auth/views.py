# coding=utf-8
from django.http import QueryDict, HttpResponseBadRequest
from django.views.generic import TemplateView, View
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.conf import settings

from oauth2client.django_orm import Storage
from apps.google_api.models import CredentialsModel
from oauth2client import xsrfutil


from forms import LoginForm

__author__ = 'dragora'


class LoginView(TemplateView):
    def get(self, request, *args, **kwargs):
        form = LoginForm()

        return render(request, 'pages/auth/login.html', {
            'form': form
        })

    def post(self, request, *args, **kwargs):
        query_dict = QueryDict(request.META['QUERY_STRING'])

        form = LoginForm(request.POST)

        if form.is_valid():
            user = authenticate(username=form.cleaned_data.get('user'), password=form.cleaned_data.get('password'))
            login(request, user)

            if 'next' in query_dict:
                next_url = query_dict['next']
            else:
                next_url = '/'

            return redirect(next_url)
        else:
            return render(request, 'pages/auth/login.html', {
                'form': form
            })


class LogoutView(TemplateView):
    def get(self, request, *args, **kwargs):
        logout(request)

        return redirect('/')


class GoogleAuthReturnView(View):
    def get(self, request, *args, **kwargs):
        if not xsrfutil.validate_token(settings.SECRET_KEY, str(request.REQUEST['state']), request.user):
            print 'Google API auth return: token invalidated!'
            return HttpResponseBadRequest()

        credential = settings.GOOGLE_API_FLOW_WEB.step2_exchange(request.REQUEST)
        storage = Storage(CredentialsModel, 'id', request.user, 'credential')
        storage.put(credential)
        return redirect("/")