# coding=utf-8
from django import forms
from django.contrib.auth import authenticate

__author__ = 'dragora'


class LoginForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)

    user = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    def clean(self):
        cleaned_data = super(LoginForm, self).clean()

        user = authenticate(username=cleaned_data.get('user'), password=cleaned_data.get('password'))
        if user is not None:
            if user.is_active:
                pass
            else:
                raise forms.ValidationError(u'Аккаунт заблокирован!')

        else:
            raise forms.ValidationError(u'Неверная пара логин\пароль!')